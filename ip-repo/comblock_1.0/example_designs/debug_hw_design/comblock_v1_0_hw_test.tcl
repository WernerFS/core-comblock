# Runtime Tcl commands to interact with - comblock_v1_0

# Sourcing design address info tcl
set bd_path [get_property DIRECTORY [current_project]]/[current_project].srcs/[current_fileset]/bd
source ${bd_path}/comblock_v1_0_include.tcl

# jtag axi master interface hardware name, change as per your design.
set jtag_axi_master hw_axi_1
set ec 0

# hw test script
# Delete all previous axis transactions
if { [llength [get_hw_axi_txns -quiet]] } {
	delete_hw_axi_txn [get_hw_axi_txns -quiet]
}


# Test all lite slaves.
set wdata_1 abcd1234

# Test: AXIL_REGS
# Create a write transaction at axil_regs_addr address
create_hw_axi_txn w_axil_regs_addr [get_hw_axis $jtag_axi_master] -type write -address $axil_regs_addr -data $wdata_1
# Create a read transaction at axil_regs_addr address
create_hw_axi_txn r_axil_regs_addr [get_hw_axis $jtag_axi_master] -type read -address $axil_regs_addr
# Initiate transactions
run_hw_axi r_axil_regs_addr
run_hw_axi w_axil_regs_addr
run_hw_axi r_axil_regs_addr
set rdata_tmp [get_property DATA [get_hw_axi_txn r_axil_regs_addr]]
# Compare read data
if { $rdata_tmp == $wdata_1 } {
	puts "Data comparison test pass for - AXIL_REGS"
} else {
	puts "Data comparison test fail for - AXIL_REGS, expected-$wdata_1 actual-$rdata_tmp"
	inc ec
}


# Test all full slaves.
set wdata_2 04040404030303030202020201010101

# Test: AXIF_DRAM
# Create a burst write transaction at axif_dram_addr address
create_hw_axi_txn w_axif_dram_addr [get_hw_axis $jtag_axi_master] -type write -address $axif_dram_addr -len 4 -data $wdata_2 -burst INCR
# Create a burst read transaction at axif_dram_addr address
create_hw_axi_txn r_axif_dram_addr [get_hw_axis $jtag_axi_master] -type read -address $axif_dram_addr -len 4 -burst INCR
# Initiate transactions
run_hw_axi r_axif_dram_addr
run_hw_axi w_axif_dram_addr
run_hw_axi r_axif_dram_addr
set rdata_tmp [get_property DATA [get_hw_axi_txn r_axif_dram_addr]]
# Compare read data
if { $rdata_tmp == $wdata_2 } {
	puts "Data comparison test pass for - AXIF_DRAM"
} else {
	puts "Data comparison test fail for - AXIF_DRAM, expected-$wdata_2 actual-$rdata_tmp"
	inc ec
}

# Test: AXIF_FIFO
# Create a burst write transaction at axif_fifo_addr address
create_hw_axi_txn w_axif_fifo_addr [get_hw_axis $jtag_axi_master] -type write -address $axif_fifo_addr -len 4 -data $wdata_2 -burst INCR
# Create a burst read transaction at axif_fifo_addr address
create_hw_axi_txn r_axif_fifo_addr [get_hw_axis $jtag_axi_master] -type read -address $axif_fifo_addr -len 4 -burst INCR
# Initiate transactions
run_hw_axi r_axif_fifo_addr
run_hw_axi w_axif_fifo_addr
run_hw_axi r_axif_fifo_addr
set rdata_tmp [get_property DATA [get_hw_axi_txn r_axif_fifo_addr]]
# Compare read data
if { $rdata_tmp == $wdata_2 } {
	puts "Data comparison test pass for - AXIF_FIFO"
} else {
	puts "Data comparison test fail for - AXIF_FIFO, expected-$wdata_2 actual-$rdata_tmp"
	inc ec
}

# Check error flag
if { $ec == 0 } {
	 puts "PTGEN_TEST: PASSED!" 
} else {
	 puts "PTGEN_TEST: FAILED!" 
}

