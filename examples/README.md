# Examples using the IP Core ComBlock

## Instructions for Zynq devices using Vivado

* Create a project for the desired board
* Add `<COMBLOCK_ROOT_PATH>/ip_repo` to the project (Settings -> IP -> Repository)
* Run in the *Tcl console* the command `source <COMBLOCK_ROOT_PATH>/examples/<BOARD_EXAMPLE>.tcl`
* Create a wrapper for the design
* Generate the Bitstream
* Export Hardware (include the Bitstream)
* Launch the SDK
* Create an Empty Baremetal/Standalone Application
* Import the needed `<COMBLOCK_ROOT_PATH>/examples/firmware/<EXAMPLE>.c` file
* Program the FPGA, configure a serial terminal and run the Application
* Enjoy :-) (and please report an issue if you found one)
