#!/bin/bash

ODIR="temp"

mkdir -p $ODIR

ghdl -a --work=fpgalib --workdir=$ODIR \
"../../ip-repo/comblock_1.0/src/mems_pkg.vhdl" \
"../../ip-repo/comblock_1.0/src/numeric_pkg.vhdl" \
"../../ip-repo/comblock_1.0/src/sync_pkg.vhdl" \
"../../ip-repo/comblock_1.0/src/FIFO.vhdl" \
"../../ip-repo/comblock_1.0/src/SimpleDualPortRAM.vhdl" \
"../../ip-repo/comblock_1.0/src/TrueDualPortRAM.vhdl" \
"../../ip-repo/comblock_1.0/src/ffchain.vhdl" \
"../../ip-repo/comblock_1.0/src/gray_sync.vhdl"

ghdl -a -P$ODIR --workdir=$ODIR \
"../AXIF_master.vhdl" \
"../../ip-repo/comblock_1.0/hdl/comblock_v1_0_AXIL_REGS.vhd" \
"../../ip-repo/comblock_1.0/hdl/comblock_v1_0_AXIF_DRAM.vhd" \
"../../ip-repo/comblock_1.0/hdl/comblock_v1_0_AXIF_FIFO.vhd" \
"../../ip-repo/comblock_1.0/src/comblock.vhd" \
"../../ip-repo/comblock_1.0/hdl/comblock_v1_0.vhd" \
"../axi_comblock_tb.vhdl"

ghdl -e -P$ODIR --workdir=$ODIR axi_comblock_tb

ghdl -r -P$ODIR --workdir=$ODIR axi_comblock_tb --vcd=axi_comblock_tb.vcd --ieee-asserts=disable-at-0
